<?php 
include("dbConnexion.php");
$tableAvion="CREATE TABLE avion (
    icao VARCHAR(40) NOT NULL  PRIMARY KEY,
    longitude double,
    latitude double,
    callsign VARCHAR(40),
    baroAltitude varchar(40)
    )" ;

try{
    $tableCreation=$mysqlConnection->prepare($tableAvion);
    $tableCreation->execute();
    }catch(Exception $e){}

$tableWeather="
CREATE TABLE weather (
    id int unsigned not null AUTO_INCREMENT PRIMARY key,
    temp varchar(50),
    temperature varchar(10),
    localisation varchar(80),
    avion_id VARCHAR(40) not null,
    CONSTRAINT fk_avion_weather
    FOREIGN key(avion_id)
    REFERENCES avion(icao) ON DELETE CASCADE
    )" ;

try{
    $creationWeather=$mysqlConnection->prepare($tableWeather);
    $creationWeather->execute();
}catch(Exception $e){}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>icao24 for city weather </title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" 
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" 
    crossorigin="anonymous"></script>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <div  class="text-center pt-4">
        <h2 class="bg-primary">Details of icao24 of flights and it's corresponding weather</h2>
    </div>
    <div class="row">

        <div id="icao24" class="col-4  m-5 ">

        </div>

        <div class="col sticky-top">
            <div class=" m-1 pt-5 sticky-top">
                
                   
                    <div class="bloc-info">
                        <p class="temps fs-3"></p>
                        <p class="temperature fs-3"></p>
                        <p class="localisation fs-3"></p>
                </div>
            </div>

        </div>
        <div class="col-4 m-4"  id="loadList" >
         

        </div>

    </div>

    <script type="module" src="main.js"></script> 
   
</body>

</html>