<?php 
include("dbConnexion.php");

$icao=$_POST["flight"][0];
$long=$_POST["flight"][1];
$lat=$_POST["flight"][2];
$callsign=$_POST["flight"][3];
$baro_altitude=$_POST["flight"][4];

$temp=$_POST["temp"];
$temperature=$_POST["temperature"];
$localisation=$_POST["localisation"];

$sqlQueryAvion="INSERT INTO avion (icao,longitude,latitude,callsign,baroAltitude) 
VALUES(?,?,?,?,?)";
$sqlQueryWeather="INSERT INTO weather(temp,temperature,localisation,avion_id) 
VALUES(?,?,?,?)";

try{
$start1=$mysqlConnection->prepare($sqlQueryAvion);
$start2=$mysqlConnection->prepare($sqlQueryWeather);
if ($start1->execute([$icao,$long,$lat,$callsign,$baro_altitude]) && 
$start2->execute([$temp,$temperature,$localisation,$icao])){

    $output=array(
        "icao" => $icao,
        "longitude" => $long,
        "latitude" => $lat,
        "callsign" => $callsign,
        "baroAltitude" => $baro_altitude,
        "temp" => $temp,
        "temperature" => $temperature,
        "localisation" => $localisation
    );
         }
}catch(Exception $e){ die("Erreur".$e->getMessage());}

echo json_encode($output);

?>