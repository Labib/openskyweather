const CLEAPI = "2c35f8e6dfa576e3adc79ad36463743a";
let resultatsAPI;
const temps = document.querySelector('.temps');
const temperature = document.querySelector('.temperature');
const localisation = document.querySelector('.localisation');

let url = "https://opensky-network.org/api/states/all";


getICAO24(url);
getDataFromDb();


//--------------------------------------------------------------
function getICAO24(url) {

  fetch(url).then((res) =>
    res.json())
    .then(function (data) {
      let stopIteration = (data.states.length <= 20) ? data.states.length : 20;
      for (let i = 0; i < stopIteration; i++) {
       
        let flightDetail=[];
        let icao = data.states[i][0];
        let long = data.states[i][5];
        let lat = data.states[i][6]
        let callsign = data.states[i][1];
        let baro_altitude = data.states[i][7];

        flightDetail.push(icao);
        flightDetail.push(long);
        flightDetail.push(lat);
        flightDetail.push(callsign);
        flightDetail.push(baro_altitude);
        
        const container = document.createElement("div");
        const icaoElement = document.createElement('button');

        icaoElement.setAttribute("data-toggle", "tooltip");
        icaoElement.setAttribute("title", "click to see the correspondingweather");
        icaoElement.setAttribute("type", "button");
        icaoElement.setAttribute("class", "btn btn-outline-primary w-100");

        const lab = document.createElement('h4');
        lab.setAttribute("class", "d-inline-block m-3");
        container.append(icaoElement, lab);
        
        lab.innerHTML = "icao24 : "+icao+" longitude : " + long +" / latitude : " 
        + lat + " callsign : " + callsign + " / baro_altitude : " + baro_altitude;

        icaoElement.innerHTML = "Sauvegarder les données";
        icaoElement.onclick = function () { 
          getCity(lat, long,flightDetail);
        }
        
        document.querySelector("#icao24").appendChild(container);
        
      }
    }).catch((err) => { console.log("erreur  : " + err); })


    

}
//---------------------------------------------------------------------

function getCity(lat, long,flightDetail) {
  fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${long}&exclude=minutely&units=metric&lang=fr&appid=${CLEAPI}`)
    .then((reponse) => {
      return reponse.json();
    })
    .then((data) => {
      resultatsAPI = data;
      temps.innerText = resultatsAPI.current.weather[0].description;
      temperature.innerText = `${Math.trunc(resultatsAPI.current.temp)}°`;
      localisation.innerText = resultatsAPI.timezone;

      let dataWeather={
        temp : resultatsAPI.current.weather[0].description,
        temperature : `${Math.trunc(resultatsAPI.current.temp)}`,
        localisation : resultatsAPI.timezone,
        flight:flightDetail

      }

      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: dataWeather,
        success: function (res) {
          data=JSON.parse(res);          
          let display=document.createElement("div");
          document.querySelector("#loadList").appendChild(display);
           display.setAttribute("class", "d-inline-block m-3 fs-6 border border-primary");
          
          display.innerHTML="icao: "+data["icao"]+" / Longitude:  "+data["longitude"]+" / latitude : "
                              +data["latitude"]+" / callsign : "+data["callsign"]+" / baro_alitude : "+data["baroAltitude"]+"<br/>"+
                            " localisation : "+data["localisation"]+" / temp : "+data["temp"]+" / temperature : "+data["temperature"]+"<br/>";
  
        }
      });
          })
      }
//-----------------------------------------------------------------------------------------------------------

function getDataFromDb(){

$.ajax({
  type: "POST",
  url: "ajaxRead.php",
  data: "data",
  dataType: "json",
  success: function (data) {
   for(let i=0;i<data.length;i++){
      let icao=data[i]["icao"];
      let longitude=data[i]["longitude"];
      let latitude=data[i]["latitude"];
      let localisation=data[i]["localisation"];
      let callsign =data[i]["callsign"];
      let baro_altitude = data[i]["baroAltitude"];
      let temp=data[i]["temp"];
      let temperature=data[i]["temperature"];
      let affiche=document.createElement("div");
      document.querySelector("#loadList").appendChild(affiche);
      affiche.setAttribute("class", "d-inline-block m-3 fs-6 border border-primary");
      
      affiche.innerHTML="icao: "+icao+" / Longitude:  "+longitude+" / latitude : "
                          +latitude+" / callsign : "+callsign+" / baro_alitude : "+baro_altitude+"<br/>"+
                        " localisation : "+localisation+" / temp : "+temp+" / temperature : "+temperature+"<br/>";
      
   }
  }
});
}
